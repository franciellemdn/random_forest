'''
  Programa de Pos-Graducao em Computacao - UFRGS

  Trabalho 1: Implementar o algoritmo de Florestas Aleatorias para tarefas
              de classificacao seguindo o paradigma de aprendizado ensemble.

  Especificacao: A solucao proposta deve:
                 - Utilizar como criterio de selecao Ganho de Informacao, tratando

  Plataforma: Python 3.5 executando em Ubuntu 14.04 Trust

  Autoras: Carolina
          Francielle
          Vanessa Borba de Souza

'''

'''Bibliotecas para manipulacao de dados (pd) e tipos (np)'''
import math
import pandas as pd
import numpy as np
import random
from sklearn.metrics import f1_score, recall_score, precision_score, accuracy_score
import sys
import time
from sklearn.metrics import confusion_matrix

'''Constantes do Sistema'''
K_FOLDS = 3
Aleatorio = 1

'''
  @Function		GeraClassesAtribNumerico

  @Brief		Funcao verifica se atributo eh numerico e,
                nesse caso, retorna os possiveis candidatos (classes)
                que melhor dividem o conj. de dados.

  @Input Parameters

  @param   		dataset - base de dados em teste.
  @param   		atributo - coluna para analise do tipo de dado

  @return		Retorna serie contendo os possiveis candidatos
                para divisao dos dados no formato 
                [NomeAtributo_>_Limiar, limiar]
'''


def GeraClassesAtribNumerico(dataset, atributo):
    dt_order = dataset.sort_values(atributo)
    #   dt_order = dataset.sort(atributo)

    lstIndex = []
    lstData = []
    clsAtribAlvo = ""
    row_aux = 0
    attrAlvo = dataset.columns[len(dataset.columns) - 1]

    for i, row in dt_order.iterrows():
        if clsAtribAlvo != row[attrAlvo] and clsAtribAlvo != "":
            clsAtribAlvo = row[attrAlvo]
            value = (row_aux + row[atributo]) / 2
            atrib = atributo + "_>_" + str(value)

            if not atrib in lstIndex:
                lstIndex.append(atrib)
                lstData.append(value)
        else:
            clsAtribAlvo = row[attrAlvo]
            row_aux = row[atributo]

    return pd.Series(lstData, lstIndex)


'''A validacao_cruzada_estratificada e uma variacao de k-fold que retorna dobras estratificadas: 
   cada conjunto contem aproximadamente a mesma porcentagem de amostras de cada classe alvo que 
   o conjunto completo.'''

'''
  @Function		GeraFolds

  @Brief		Funcao que separa a lista de elementos em grupos (folds) pela quantidade de partes/split

  @Input Parameters

  @param   		lista - lista com os valores gerados randomicamente indicando o indice da linha/row do dataset
  @param   		partes - quantidade de partes que tera os folds

  @return		retorna uma lista de resultados/folds 
'''


def GeraFolds(lista, partes):
    splited = []
    len_l = len(lista)
    for i in range(partes):
        start = int(i * len_l / partes)
        end = int((i + 1) * len_l / partes)
        splited.append(lista[start:end])
    # print(splited)
    return splited


'''
  @Function		GeraAleatorio

  @Brief		Funcao que gera uma lista com valores aleatorios 
  @Input Parameters

  @param   		tam - tamanho da lista que devera ser gerada, corresponde ao tamanho da group.dataset analisado
  @param   		reposicao - True, para amostragem com reposicao
  @return		retorna uma lista de numeros aleatorios dentro de um range 
'''


def GeraAleatorio(tam, reposicao):
    result = []
    while len(result) < tam:
        r = random.randint(0, tam - 1)
        if reposicao == True:
            result.append(r)
        else:
            if r not in result:
                result.append(r)

    return result


'''
  @Function		GeraDataFolds

  @Brief		Funcao que gera uma lista de datasets com os folds
  @Input Parameters

  @param   		dataset - grupo/dataset que esta sendo analisado
  @param        lstFolds - uma lista de resultados/folds
  @return		retorna uma lista contendo os dados dos folds
'''


def GeraDataFolds(dataset, lstFolds):
    lstDataset = []
    for i in lstFolds:
        lstDataset.append(dataset.iloc[i])
    return lstDataset


'''
  @Function		GeraTrainTest

  @Brief		Funcao que gera os conjuntos de treino e teste com base na validacao_cruzada
  @Input Parameters

  @param   		folds -  conjunto de folds 
  @return		Uma lista contendo dados para treino e teste (se K_FOLDS for 3, havera 3 conjuntos treino e teste)
'''


def GeraTrainTest(folds):
    conjuntos = []
    test = []
    for i in range(len(folds)):
        frames = []
        for j in range(len(folds)):
            if i == j:  # define o que ira ficar para teste
                test = folds[i]
            else:
                frames.append(folds[j])  # define o que ira ficar para treino

        # concatena
        train = pd.concat(frames, ignore_index=True)

        # armazena cada conjunto de treino e teste em uma lista kk
        conjuntos.append([train, test])

    return conjuntos


'''
  @Function		EstratificaFolds

  @Brief		Funcao que executa o processo de estratificar os dados em folds 
  @Input Parameters

  @param   		dataset - dataset principal
  @return		no futuro saberemos
'''


def EstratificaFolds(dataset):
    atrAlvo = dataset.columns[len(dataset.columns) - 1]
    lstDataFolds = []
    for name, group in dataset.groupby(atrAlvo):
        # print(name)
        # print(group)
        result = GeraAleatorio(group.shape[0], False)
        # print (result)
        lstFolds = GeraFolds(result, K_FOLDS)
        # print (lstFolds)
        lstDataFolds.append(GeraDataFolds(group, lstFolds))

    folds = []
    for i in range(K_FOLDS):
        frames = []
        for j in lstDataFolds:
            # print j[i]
            frames.append(j[i])
        folds.append(pd.concat(frames, ignore_index=True))

    return folds


'''
  @Function		MetricasDesempenho

  @Brief		Funcao que realizara a avaliacao da arvore a partir do conjunto de teste.
                Passo 1: Realizar o test e computar os acertos e erros
                Passo 2: Realizar a F-Measure para avaliar
  @Input Parameters

  @param   		tree - arvore gerada
  @param        test - conjunto de teste
  @return		no futuro saberemos
'''


def MetricasDesempenho(y_true, y_pred):
    recall = [0.0]
    precision = [0.0]
    fMeasure = [0.0]
    acuracia = [0.0]

    # matrix = confusion_matrix(y_true,y_pred)
    # for i in range(len(y_true)):
    #     print (y_true[i],y_pred[i])
    fMeasure = f1_score(y_true, y_pred, average=None)
    recall = recall_score(y_true, y_pred, average=None)
    precision = precision_score(y_true, y_pred, average=None)
    acuracia = accuracy_score(y_true, y_pred)
    # print("A FMeasure e:",fMeasure)
    # print("Recall e:",recall)
    # print("Precision e:",precision)

    return recall, precision, fMeasure, acuracia


'''
  @Function		BuscaDadosTest

  @Brief		Funcao que recebe como entrada a arvore e o  conjunto de teste
  @Input Parameters

  @param   		test - conjunto de instancias de teste
  @param        floresta de arvores 
  @return		Listas - y_true (Classificacao esperada), y_pred (Classificacao Predita)
'''


def BuscaDadosTest(test, floresta):
    y_pred = []
    y_true = []
    if (not test.empty):
        for index, row in test.iterrows():
            y_true.append(row[row.index[len(row) - 1]])
            row = row.drop(row.index[len(row) - 1])
            classeFinal = BuscaFlorestaAleatoria(row,
                                                 floresta)  # Funcao que faz a busca da instancia/row para todas as tree e retorna
            # a resposta pela votacao majoritaria
            if (classeFinal == None):
                # fx = 'Classe_Desconhecida'
                # y_test.append(fx)
                print("Classe Desconhecida:", row)
                y_true.pop()
            else:
                y_pred.append(classeFinal)

    return y_true, y_pred


'''
  @Function		ValidacaoCruzada

  @Brief		Funcao que realizara todo o processo de validacao_cruzada.
                Passo 1: Gerar folds
                Passo 2: Gerar todas as combinacoes de conjuntos train e test
                Passo 3: Gerar a arvore de decisao para cada train e realizar o test
                Passo 4: Realizar a F-Measure para avaliar
  @Input Parameters

  @param   		dataset - dataset principal
  @return		no futuro saberemos
'''


def ValidacaoCruzada(dataset, nArvores):
    folds = EstratificaFolds(dataset)
    conjuntos = GeraTrainTest(folds)
    recallSum = []
    precisionSum = []
    fMeasureSum = []
    accSum = []
    lstFloresta = []

    for train, test in conjuntos:
        floresta = GeraFlorestaAleatoria(train,
                                         nArvores)  # Gera Floresta com base no conjunto Train da Validacao Cruzada (Lista com varias tree)
        y_true, y_pred = BuscaDadosTest(test,
                                        floresta)  # Gera as predicoes para o conjunto de test na Floresta com Votacao majoritaria

        if (y_true and y_pred):
            recall, precision, fMeasure, acc = MetricasDesempenho(y_true,
                                                                  y_pred)  # Calcula o desempenho para aquela floresta gerada de train
            fMeasureSum.append(fMeasure)
            precisionSum.append(precision)
            accSum.append(acc)
            recallSum.append(recall)
            lstFloresta.append([floresta, fMeasure])
    stdA = [np.std(fMeasureSum), np.std(precisionSum), np.std(accSum), np.std(recallSum)]
    meanA = [np.mean(fMeasureSum), np.mean(precisionSum), np.mean(accSum), np.mean(recallSum)]

    result = pd.DataFrame(dict(std=stdA, mean=meanA), index=['FMeasure', 'Precision', 'Acuracia', 'Recall'])

    return result, lstFloresta


'''
  @Function		BootstrapTrainTest

  @Brief		Funcao que a partir da lista de index aleatoria,
                e conjunto de dados original, gera um conj de
                treinamento do tamanho do original e um conj de
                teste com as amostras nao inclusas no conj de 
                treinamento.

  @Input Parameters

  @param   		dataset - dataset original
  @return		Conjunto de treinamento
'''


def BootstrapTrainTest(idx_amostras, dts_original):
    train = pd.DataFrame(dts_original, idx_amostras)
    # test = pd.concat([dts_original, train]).drop_duplicates(keep=False)
    # print("TRAIN\n",train)
    # print("TESTE\n",test)
    return train


'''
  @Function		Bootstrap

  @Brief		Funcao que realiza amostragem com reposicao

  @Input Parameters

  @param   		dataset - dataset original
  @return		Conjunto de Treinamento e Teste
'''


def Bootstrap(dts_original):
    idx_amostras = GeraAleatorio(dts_original.shape[0], True)
    return BootstrapTrainTest(idx_amostras, dts_original)


'''
  @Function		EntropiaAtrCategoria

  @Brief		Funcao calcula entropia para atributo
                com valor categoria.

  @Input Parameters

  @param   		dataset - base de dados em teste.
  @param   		lst - coluna para analise do tipo de dado
  @param   		entropAlvo - coluna para analise do tipo de dado
  @param   		idx - coluna para analise do tipo de dado

  @return		Retorna o maior ganho de informacao entre classes
                testadas para o atributo
'''


def EntropiaAtrCategoria(dataset, lst, entropAlvo, idx):
    # Quantidade de classes no ultimo atributo
    tamLst = len(lst) - 1
    qt = lst[tamLst].shape[0]

    entroResult = 0.0

    for j in range(len(lst[idx])):
        atributo = dataset.columns[idx]
        vlrAtributo = lst[idx].index[j]
        atrAlvo = dataset.columns[len(dataset.columns) - 1]
        soma = 0
        pI = 0

        for k in range(qt):
            vlrAlvo = lst[tamLst].index[k]
            # dadosYes = (dataset[(dataset["Tempo"] == "Ensolarado") & (dataset["Joga"] == "Sim")]) - faz essa busca
            dados = (dataset[(dataset[atributo] == vlrAtributo) & (dataset[atrAlvo] == vlrAlvo)])
            # print(attr, value, attrAlvo, valueAlvo, dados.shape[0],lst[i][j])
            piAux = float(dados.shape[0] / float(lst[idx][j]))
            if (piAux > 0):  # Impede que haja valores 0 ou menores
                pI = -piAux * np.log2(piAux)
            soma = pI + soma

        ftMult = float(lst[idx][j] / float(sum(lst[
                                                   tamLst])))  # Fator Multiplicativo e a probabilidade de ser o atributo/valor/instancia dentre todos os existentes = (5/14),(4/14),(5/14) do Benchmark
        # print (ftMult)
        # print (attr,value,soma,lst[i][j],sum(lst[4]))
        entroResult = ftMult * (soma) + entroResult  # Calculo da entropia media
        # print (entroResult)
        # entropia.append([atributo,vlrAtributo,soma,ftMult,ftMult*(soma)])
        ganho = entropAlvo - entroResult

    return ganho


'''
  @Function		EntropiaAtrNumero

  @Brief		Funcao calcula entropia para atributo com valor numerico. 
                Cada possivel candidato a divisao do conjunto de dados eh 
                tratado como uma classe do atributo. Exemplo: se temperatura
                for numerica, tem-se as classes Temperatura_>_Limiar1, 
                Temperatura_>_Limiar2... A funcao testa para cada valor do 
                atributo alvo, a entropia das duas classes >Limiar e <= Limiar,
                calcula o ganho de cada classe e retorna a classe que maximiza
                o ganho de informacao.


  @Input Parameters

  @param   		dataset - base de dados em teste.
  @param   		lst - lista de valores possiveis para cada atributo
  @param   		entropAlvo - entropia da classe alvo
  @param   		idx - indice da coluna (atributo) em analise no dataset

  @return		Retorna o maior ganho de informacao e indice para localizar
                limiar selecionado em lst
'''


def EntropiaAtrNumero(dataset, lst, entropAlvo, idx):
    # Quantidade de classes no ultimo atributo
    tamLst = len(lst) - 1
    qt = lst[tamLst].shape[0]

    entroResult = 0.0
    ganho = 0.0
    limiarAtributo = 0.0
    # indiceLimiar = 0

    for j in range(len(lst[idx])):
        atributo = dataset.columns[idx]
        atrAlvo = dataset.columns[len(dataset.columns) - 1]
        limiar = float(lst[idx].values[j])

        totRegra = float(dataset[(dataset[atributo] > limiar)].shape[0])
        totContraRegra = float(dataset[(dataset[atributo] <= limiar)].shape[0])

        somaRegra = 0
        somaContraRegra = 0
        pI = 0

        # Testa dados do atributo para valores do atributo alvo
        for k in range(qt):
            vlrAlvo = lst[tamLst].index[k]

            # Calcula o somatorio de info relativo a cada classe alvo
            # para a classe > limiar
            dados = (dataset[(dataset[atributo] > limiar) & (dataset[atrAlvo] == vlrAlvo)])
            if (totRegra > 0):
                probClasse = float(float(dados.shape[0]) / (float)(totRegra))
                if (probClasse > 0):
                    pI = -probClasse * np.log2(probClasse)
                    somaRegra = pI + somaRegra

            # Calcula o somatorio de info relativo a cada classe alvo
            # para a classe <= limiar
            dados = (dataset[(dataset[atributo] <= limiar) & (dataset[atrAlvo] == vlrAlvo)])
            if (totContraRegra > 0):
                probClasse = float(float(dados.shape[0]) / float(totContraRegra))
            if (probClasse > 0):
                pI = -probClasse * np.log2(probClasse)
            somaContraRegra = pI + somaContraRegra

        entroResult = (totRegra / float(dataset.shape[0])) * (somaRegra) + \
                      (totContraRegra / float(dataset.shape[0])) * (somaContraRegra)

        # se ganho anterior eh menor que atual, assume o limiar atual
        if (ganho < (entropAlvo - entroResult)):
            ganho = entropAlvo - entroResult
            limiarAtributo = limiar
            # indiceLimiar = j

    return ganho, limiarAtributo


'''
  @Function		EntropiaAlvo

  @Brief		Funcao calcula entropia para atributo alvo. 

  @Input Parameters

  @param   		dataset - base de dados em teste.

  @return		lst - Lista com valores possiveis para cada atributo
                entropAlvo - Valor da entropia para classe alvo
                totClasses - tamanho de lst (total de valores dos atributos)
'''


def EntropiaAlvo(dataset):
    entropAlvo = 0
    lst = []
    # print (dataset)
    #########################################
    # contabiliza quantos elementos possuem cada valor de atributo e armazena em uma lista
    # vIdxAtrubutos = SelecaoAleatoriaAtributos(dataset)
    # for i in vIdxAtrubutos:
    for attr in dataset.columns:
        # attr = dataset.columns[i]
        # Testa o tipo de dado do atributo e monta lista com a contagem de cada valor
        # para cada atributo. Por exemplo: No atributo Temperatura - Quente sao 4 valores, amena 6 valores...
        # Para dados numericos, cria uma classe com o total do label. Ex: temperatura_>_30, sao 4
        if np.issubdtype(dataset[attr].dtype, np.number):
            lst.append(GeraClassesAtribNumerico(dataset, attr))
        else:
            lst.append(pd.value_counts(dataset[attr]))
    # print (attr)
    #########################################
    # Calculando a entropia do Pai/Raiz da sub-arvore
    tamLst = len(lst) - 1
    totClasses = lst[tamLst].shape[0]  # calcula a quantidade de classes no ultimo atributo
    for k in range(totClasses):
        piAux = (float(lst[tamLst][k]) / float(sum(lst[tamLst])))
        # print (lst[tamLst][k],"-----",sum(lst[tamLst]),piAux)
        if (piAux > 0):
            pI = -piAux * np.log2(piAux)
            entropAlvo = pI + entropAlvo
    print("Entropia do Alvo:", entropAlvo)

    return lst, entropAlvo, totClasses


'''
  @Function		CalculoGanhoInfo

  @Brief		Funcao calcula o ganho de informacao para cada atributo. 

  @Input Parameters

  @param   		dataset - base de dados em teste.
                lst - Lista com valores possiveis para cada atributo
                entropAlvo - Valor da entropia para classe alvo

  @return		Lista com atributos e respectivos ganhos calculados
'''


def CalculoGanhoInfo(dataset, lst, entropAlvo):
    lstGanho = []
    limiarAtributo = 0.0

    # calculando a entropia para cada atributo selecionado aleatoriamente
    if (Aleatorio == 1):
        vIdxAtrubutos = SelecaoAleatoriaAtributos(dataset)
    else:
        vIdxAtrubutos = range(len(lst) - 1)
    for i in vIdxAtrubutos:
        # for i in range(len(lst) - 1):
        atributo = dataset.columns[i]

        if np.issubdtype(dataset[atributo].dtype, np.number):
            ganho, limiarAtributo = EntropiaAtrNumero(dataset, lst, entropAlvo, i)
        else:
            limiarAtributo = 0
            ganho = EntropiaAtrCategoria(dataset, lst, entropAlvo, i)

        lstGanho.append([atributo, limiarAtributo, ganho])
        # print (attr,entroResult,ganho)

    return pd.DataFrame.from_records(lstGanho, columns=['Atributo', 'Limiar Atributo Num', 'Ganho'])


'''
  @Function		SubgrupoAtrNumerico

  @Brief		Funcao retorna subgrupo para filtro numerico

  @Input Parameters

  @param   		dataset - base de dados em teste.
  @param   		atributo - valor atributo.
  @param   		filtro - regra de filtro para dataset conforme limiar.

  @return		Subgrupo
'''


def SubgrupoAtrNumerico(dataset, atributo, filtro):
    group = dataset.query(filtro)
    group = group.drop(atributo, axis=1)
    return group


'''
  @Function		ClasseMajoritaria

  @Brief		Funcao analisa a classe majoritaria entre as duas restantes
                que devera compor o no folha.

  @Input Parameters

  @param   		totClasses - Total de classes alvo restantes no subgrupo.
  @param   		lst        - lst - Lista com valores possiveis para cada atributo.

  @return		Retorna RotuloClasseMajoritaria, PercentualClasseMajoritaria
'''


def ClasseMajoritaria(totClasses, lst):
    if (totClasses == 1):
        return lst[len(lst) - 1].index[0], float(100)
    if (totClasses == 2):
        totRegistros = len(lst) - 1
        totClasse_1 = float(lst[totRegistros][0])
        totClasse_2 = float(lst[totRegistros][1])
        totalRegistros = totClasse_1 + totClasse_2

        if (totClasse_1 > totClasse_2):
            return lst[len(lst) - 1].index[0], float(totClasse_1 / totalRegistros)
        else:
            return lst[len(lst) - 1].index[1], float(totClasse_2 / totalRegistros)

    return "", 0


'''
  @Function		InducaoArvore

  @Brief		Funcao recursivamente monta as regras para uma arvore.

  @Input Parameters

  @param   		dataset - base de dados em teste.

  @return		Nulo
'''


# lstAux = []
def InducaoArvore(dataset):
    atributoSelecionado = " "
    print(" ")
    # se houver apenas uma coluna, essa coluna seria a alvo, e por isso nao e necessario mais continuar a recursao
    if (len(dataset.columns) <= 1):
        # print("Acabou")
        lst, entropAlvo, totClasses = EntropiaAlvo(dataset)
        classeNodo, percClasse = ClasseMajoritaria(totClasses, lst)
        print("Crie um no folha: ", [classeNodo, percClasse, 1])

        return [classeNodo, percClasse, 1]
    else:
        lst, entropAlvo, totClasses = EntropiaAlvo(dataset)
        # se a entropAlvo for 0, quer dizer que tem apenas uma classe naquele conjunto de dados
        if ((entropAlvo <= 0) or (totClasses == 1)):
            classeNodo, percClasse = ClasseMajoritaria(totClasses, lst)
            print("Crie um no folha: ", [classeNodo, percClasse, 1])

            return [classeNodo, percClasse, 1]
        else:
            dfGanho = CalculoGanhoInfo(dataset, lst, entropAlvo)
            print(dfGanho)

            if (not dfGanho.empty):
                # seleciona o maior valor do conjunto de ganhos dos atributos
                attrSel = dfGanho[dfGanho['Ganho'] == [max(dfGanho['Ganho'])]]
                print("Atributo Selecionado:", attrSel.values[0][0])

                treeT = {attrSel.values[0][0]: {}}
                ganhoAtrrSel = round(max(dfGanho['Ganho']), 3)

                # Testa se ganho e zero e temos duas classes com baixo erro, entao cria no folha nao puro
                classeNodo = ClasseMajoritaria(totClasses, lst)
                if (attrSel.values[0][2] <= 0) and (classeNodo != ""):
                    classeNodo, percClasse = ClasseMajoritaria(totClasses, lst)
                    print("Crie um no folha: ", [classeNodo, percClasse, ganhoAtrrSel])

                    return [classeNodo, percClasse, ganhoAtrrSel]
                else:
                    if np.issubdtype(dataset[attrSel.values[0][0]].dtype, np.number):
                        filtroLimiar = str(attrSel.values[0][0]) + " > " + str(attrSel.values[0][1])
                        # print(filtroLimiar)
                        group = SubgrupoAtrNumerico(dataset, attrSel.values[0][0], filtroLimiar)

                        lstLimiares = [attrSel.values[0][0], attrSel.values[0][1], filtroLimiar]
                        # lstAux.append(lstLimiares)

                        subtree = InducaoArvore(group)
                        treeT[attrSel.values[0][0]][filtroLimiar] = subtree

                        filtroLimiar = str(attrSel.values[0][0]) + " <= " + str(attrSel.values[0][1])
                        group = SubgrupoAtrNumerico(dataset, attrSel.values[0][0], filtroLimiar)

                        lstLimiares = [attrSel.values[0][0], attrSel.values[0][1], filtroLimiar]
                        # lstAux.append(lstLimiares)
                        subtree = InducaoArvore(group)
                        treeT[attrSel.values[0][0]][filtroLimiar] = subtree
                    else:
                        # attrSel.values[0][0] Captura o nome do atributo por ex. 'Ventoso'
                        for name, group in dataset.groupby(attrSel.values[0][0]):
                            # remove do grupo a coluna do atributo corrente (coluna: axis = 1 / linha: axis=0)
                            group = group.drop(attrSel.values[0][0], axis=1)
                            # print('o name e'+name)
                            # print('O group e'+group)
                            subtree = InducaoArvore(group)
                            treeT[attrSel.values[0][0]][name] = subtree

        return treeT


'''
@Function		TestaKeyCondicional

  @Brief		Funcao testa se chave de pesquisa eh teste condicional
                ou nome de atributo
  @Input Parameters

  @param   		key  - Valor chave de pesquisa na arvore
  @return		True - se sentenca eh um teste condicional para
                          atributo numerico
'''


def TestaKeyCondicional(key):
    # testa se key possui sinal de > ou <
    if ('>' in key):
        return True
    elif ('<' in key):
        return True
    else:
        return False


'''
@Function		TrataKeyCondicional

  @Brief		Funcao extrai o nome do atributo da sentenca
                string informada
  @Input Parameters

  @param   		key - Valor chave de pesquisa na arvore
  @return		string - Nome da coluna (atributo)
'''


def TrataKeyCondicional(key):
    nomeColuna = key.split()[0]
    # retorna o nome da coluna estraido da setencao condicional sepal_widht > 2.2 (sepal_widht)
    return nomeColuna


'''
@Function		RetornaTipoColuna

  @Brief		Funcao recebe nome da coluna e testa se e texto
  @Input Parameters

  @param   		nomeColuna - Nome do atributo no dataset
  @return		retorna - True se coluna e do tipo texto
'''


def RetornaTipoColuna(nomeColuna):
    if (type(nomeColuna) is not str):
        return True
    else:
        return False


'''
@Function		BuscaArvore

  @Brief		Funcao que realiza a busca em uma arvore ate o noFolha e devolve a classificacao
  @Input Parameters

  @param   		dataset - grupo/dataset que esta sendo analisado
  @param        dfBusca - Serie contendo os valores que ira buscar a arvore 
  @return		retorna uma lista contendo os dados dos folds
'''


def BuscaArvore(dfBusca, tree):
    keys = tree.keys()
    raiz = 0
    # print(tree)
    for i in keys:
        valueInst = dfBusca[i]
        raiz = i
    segueBusca = True
    while (segueBusca):
        tree = tree.get(raiz)
        # print(tree)
        # print("Filho de ", raiz, tree)
        if isinstance(tree, (list)):
            segueBusca = False
        elif tree is None:
            print("Nao foi possivel classificar a instancia")
            segueBusca = False
        else:
            keys = tree.keys()
            # print(keys)
            for i in range(len(dfBusca)):
                # Se o dfBusca[i] que estou avaliando e numerico, e a raiz anterior e uma sentenca, e a posicao de dfBusca contem o mesmo indice da sentenca
                if ((RetornaTipoColuna(dfBusca[i]))):  # e numerico
                    # print("E numerico",dfBusca[i])
                    # preciso olhar todas as chaves e verificar qual sera a proxima raiz
                    # and (TrataKeyCondicional(raiz) == dfBusca.index[i])
                    # and (TestaKeyCondicional(raiz))
                    for k in keys:
                        atributoKey = TrataKeyCondicional(
                            k)  # captura da sentenca o atributo petal_width > 8.1 (recupera petal_width)
                        # verifica se esse atributo e o mesmo do dfBusca[i]
                        if (atributoKey == dfBusca.index[i]):
                            # print("Escolhi Numerico",atributoKey)
                            sentenca = k.replace(str(atributoKey), str(dfBusca[i]))
                            # print("E igual")
                            if (eval(sentenca)):  # e um caminho que devemos seguir na arvore e gerar sub arvore
                                raiz = k
                                # print("Minha nova raiz e",raiz)
                                break
                else:
                    if ((dfBusca[i] in keys)):
                        raiz = dfBusca[i]
                        # print("Escolhi:", raiz)
                        break
                    elif (dfBusca.index[i] in keys):
                        # print("Esta presente")
                        raiz = dfBusca.index[i]
                        # print("Escolhi:", raiz)
                        break

    return tree


'''
@Function		VotacaoMajoritaria

  @Brief		Funcao recebe o vetor com o resultado da classificacao de cada
                arvore no ensemble e retorna a classe mais mencionada
  @Input Parameters

  @param   		vector - grupo/dataset que esta sendo analisado
  @param        dfBusca - Serie contendo os valores que ira buscar a arvore 
  @return		Classe predita resultante da votacao
'''


def VotacaoMajoritaria(vetorClasses):
    if (vetorClasses):
        A = np.array(vetorClasses)
        classes, pos = np.unique(A, return_inverse=True)
        counts = np.bincount(pos)
        posClasse = counts.argmax()
        # print('A classe escolhida foi:', classes[posClasse])
        return classes[posClasse]


'''
@Function		SelecaoAleatoriaAtributos

  @Brief		Funcao gera um vetor com os atributos aletoriamente 
                selecionados. 
                Tamanho do vetor = sqrt(totalAtributosDataSet)

  @Input Parameters

  @param   		dataset - conjunto de dados antes da selecao de atributos
  @return		vAtributos - Retorna o vetor com indices de atributos selecionados
'''


def SelecaoAleatoriaAtributos(dataset):
    vIdxAtributos = []

    lst = GeraAleatorio(dataset.shape[1] - 1, False)
    totalColunas = int(math.sqrt(dataset.shape[1]))

    for i in range(totalColunas):
        vIdxAtributos.append(lst[i])

    vIdxAtributos = sorted(vIdxAtributos)

    return vIdxAtributos


'''
@Function		CalculaRecall

  @Brief		Funcao para calculo da revocacao

  @Input Parameters

  @param   		vp - Total de Verdadeiros Positivos no conj analisado
  @param        fn - Total de Falsos Negativos no conj analisado
  @return		Num - Retorna o valor do recall calculado
'''


def CalculaRecall(vp, fn):
    if (vp > 0):
        recall = vp / (vp + fn)
    else:
        recall = 0
    return recall


'''
@Function		CalculaPrecisao

  @Brief		Funcao para calculo da precisao

  @Input Parameters

  @param   		vp - Total de Verdadeiros Positivos no conj analisado
  @param        fp - Total de Falsos Positivos no conj analisado
  @return		Num - Retorna o valor do precisao calculado
'''


def CalculaPrecisao(vp, fp):
    if (vp > 0):
        precision = vp / (vp + fp)
    else:
        precision = 0
    return precision


'''
@Function		FMeasure

  @Brief		Calcula

  @Input Parameters

  @param   		vp - Total de Verdadeiros Positivos no conj analisado
  @param        fp - Total de Falsos Positivos no conj analisado
  @return		Num - Retorna o valor do precisao calculado
'''


def FMeasure(recall, precision, beta=None):
    if beta is None:
        if (precision + recall > 0):
            return 2 * ((precision * recall) / (precision + recall))
        else:
            return 0
    else:
        if (precision + recall > 0):
            return (1 + beta ** 2) * ((precision * recall) / ((beta ** 2) * precision + recall))
        else:
            return 0


'''
@Function		GeraFlorestaAleatoria

  @Brief		Funcao gera o modelo final floresta aleatoria

  @Input Parameters

  @param        dataset - conjunto de dados original para formacao e teste do modelo
  @param   		nArvores - numero de arvores para montar a floresta
  @return		lstTree - Lista de arvores geradas
'''


def GeraFlorestaAleatoria(dataset, nArvores):
    lstTree = []
    for i in range(nArvores):
        train = Bootstrap(dataset)
        tree = InducaoArvore(train)
        lstTree.append(tree)

    return lstTree


'''
@Function		BuscaFlorestaAleatoria

  @Brief		Funcao que realiza a busca na floresta gerada

  @Input Parameters

  @param   		dfInstancia - instancia do conjunto de dados para classificacao 
  @param        floresta - modelo ensemble gerado (lista de arvores)
  @return		Classe predita pelo modelo
'''


def BuscaFlorestaAleatoria(dfInstancia, floresta):
    votacao = []
    for tree in floresta:
        resposta = BuscaArvore(dfInstancia, tree)
        if (resposta != None):
            votacao.append(resposta[0])
    classe = VotacaoMajoritaria(votacao)

    return classe


'''
@Function		EnsembleFloresta

  @Brief	    Funcao avalia o desempenho do modelo final gerado	

  @Input Parameters

  @param   		datasetOriginal - conjunto de dados inicial
  @return       floresta - modelo ensemble gerado (lista de arvores)
  @return		Matriz contendo media e desvio padrao das medidas
                FMeasure, Precisao, Recall, Acuracia
'''


def EnsembleFloresta(datasetOriginal, nArvores):
    dfMetricas, floresta = ValidacaoCruzada(datasetOriginal, nArvores)

    return dfMetricas, floresta


'''
@Function		SetupDesempenho

  @Brief	    Funcao que avaliar o Desempenho para diferentes nArvores	

  @Input Parameters

  @param   		dataset - conjunto de dados inicial
  @param        nameDataset - 
'''


def SetupDesempenho(dataset, nameDataset):

    nArvoresLst = [10, 20, 30, 40, 50]  # lista que contem diferentes numeros de nArvores
    for nArvores in nArvoresLst:
        arquivo = open('SetupDesempenhoW.txt', 'a')
        inicio = time.time()
        dfMetricas, lstFlorestas = EnsembleFloresta(dataset, nArvores)
        fim = time.time()
        tempo = fim - inicio
        strA = "Dataset: " + nameDataset + "\n" + " Tempo de Execucao" + str(float(tempo))
        arquivo.write(strA)
        strA = "nArvores: " + str(nArvores) + "\n"
        arquivo.write(strA)
        strA = "Metricas: " + str(dfMetricas) + "\n"
        arquivo.write(strA)

        arquivo.close()


'''
@Function		EscolheModeloFinal

  @Brief	    Funcao que escolhe a floresta que possui maior FMeasure
  @Input Parameters

  @param   		lstFloresta
  @return       modeloFinal
'''


def EscolheModeloFinal(lstFlorestas):
    # Seleciona a maior FMeasure entre as florestas geradas, para decidir qual sera a floresta do modelo
    maior = 0
    for lstF, fmeaS in lstFlorestas:
        if (fmeaS[0] > maior):
            maior = fmeaS[0]
            modelo = lstF

    return modelo

if __name__ == '__main__':
    # especificando o arquivo de dados e o seu respectivo delimitador
    # dataset = pd.read_csv('dadosBenchmark_num.csv', delimiter=';')
    # dataset = pd.read_csv('dadosBenchmark.csv', delimiter=';')
    # dataset = pd.read_csv('irisDataset.csv', delimiter=';')

    if (len(sys.argv) > 4):
        nArvores = int(sys.argv[1])
        K_FOLDS = int(sys.argv[2])
        Aleatorio = int(sys.argv[3])
        nameDataset = sys.argv[4]

        print("\nParametros de configuracao informados (nro arvores, k-folds, arquivo de dados):", nArvores, K_FOLDS,
              nameDataset)

    else:
        print("\nExecute: python decision_tree.py nArvores k_Folds Aleatorio nomeArquivoDataset.csv")
        print("\nOnde:\nnArvores = Quantidade de Arvores para o Ensemble")
        print("\nk_folds = Quantidade de K_Folds que serao gerados")
        print("\nAleatorio = 1 se permitir selecao de atributo aleatorio, 0 se nao")
        print("\nnomeArquivoDataset.csv = recebe o nome do arquivo que contem o dataset")

    print("\nIMPORTANTE: o algoritmo gerado espera que a coluna atributo alvo seja a ultima coluna do dataset!")

    # Para teste sem informar valores na console
    # nameDataset = "ionosphere_data.csv"
    # nArvores = 2
    # K_FOLDS = 2

    dataset = pd.read_csv(nameDataset, delimiter=';')

    dfMetricas, lstFlorestas = EnsembleFloresta(dataset, nArvores)
    print("Dataset:", nameDataset)
    print("\nModelo Floresta - Metricas Desempenho: \n", dfMetricas)

    if (nameDataset == "dadosBenchmark.csv"):
        print("Gerando Arvore para o BenchMark")
        Aleatorio = 0
        InducaoArvore(dataset)

    # A partir desse modelo, podera ser feitas predicoes
    # print("MODELO FINAL")
    # modelo = EscolheModeloFinal(lstFlorestas)
    # y_true, y_pred = BuscaDadosTest(dataset,modelo)
    # print(y_pred)

    # print("\nModelo Floresta Aleatoria Gerado:\n", lstFlorestas)
    # Para testar os dados Benchmark deve-se rodar essa funcao passando como informacao o arquivo benchmark
    # InducaoArvore(dataset)
    # SetupDesempenho(dataset,nameDataset)    SetupDesempenho(dataset, nameDataset)